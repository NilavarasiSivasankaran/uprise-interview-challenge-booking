require('dotenv').config();      //access env variables
const admin=require('firebase-admin');
const serviceAccount=require('./secret.json');

const moment=require('moment-timezone');

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount)
});

const dateString='YYYY/MM/DD';
const dateTimeString='YYYY/MM/DD HH:mm A';

const db=admin.firestore();

async function getSlots (req, res) {
    try {
        const { date, timezone }=req.body;

        const snapshot=await db.collection('events').get();
        console.log('snapshot', snapshot);
        let slotsAvailable=[];
        snapshot.forEach((doc) => {
            const hours=doc.data();
            const storeDate=hours.startHours.toDate();
            const storeDay=moment(storeDate).tz(timezone).format(dateString)
            if (storeDay===date)
                slotsAvailable.push({ id: doc.id, data: doc.data() });
        });

        if (!moment.tz.zone(timezone)) {     
            res.status(400).send({ 'message': 'Invalid Timezone' });
        } else if (slotsAvailable.length) {
            let { startHours, endHours, duration, slots }=slotsAvailable[0].data;

            let emptySlots=[];
            slots_arr=slots.map(date => moment(date.toDate()).tz(timezone).format(dateTimeString));
            startHours=startHours.toDate(), endHours=endHours.toDate();
            startHours=moment(startHours).tz(timezone);
            endHours=moment(endHours).tz(timezone);

            while (startHours.unix()<endHours.unix()) {   
                emptySlots.push(startHours.format(dateTimeString));
                startHours.add(duration, 'minutes');
            }
            emptySlots=emptySlots.filter(date => !slots_arr.includes(date));
            console.log('emptySlots', emptySlots);
            console.log('duration', duration);
            res.status(200).send({ data: { slots: emptySlots, duration } });
        } else {
            res.status(404).send({ message: 'No data available', data: [] });
        }

    } catch (err) {
        console.log('err', err);
        res.status(500).send({ message: 'something went wrong!', error: true });
    }
}

async function getEvents (req, res) {
    try {
        let { startDate, endDate }=req.body;
        startDate=moment(startDate, dateString).unix();
        endDate=moment(endDate, dateString).unix();
        if (endDate<startDate) {
            res.status(400).send({ message: 'Start date should come before end date', error: true })
        } else {
            const snapshot=await db.collection('events').get();
            let events=[];
            snapshot.forEach((doc) => {
                const hours=doc.data();
                let storeStart=hours.startHours.toDate();
                storeStart=moment(storeStart).startOf('day').unix()
                if (startDate<=storeStart&&endDate>=storeStart)
                    events.push(hours)
            })
            if (events.length) {
                res.status(200).send({ data: events });
            } else {
                res.status(404).send({ message: 'No data available', data: [] });
            }
        }
    } catch (err) {
        console.log(err)
        res.status(500).send({ message: 'something went wrong!', error: true });
    }

}

async function createEvent (req, res) {
    try {
        let { dateTime, duration }=req.body;
        dateTime=moment(dateTime);
        console.log('dateTime', dateTime);
        const snapshot=await db.collection('events').get();
        let event=[], match=false;
        snapshot.forEach((doc) => {
            const hours=doc.data();
            let storeStart=hours.startHours.toDate();
            let storeEnd=hours.endHours.toDate();
            storeEnd=moment(storeEnd);
            storeStart=moment(storeStart)
            if (dateTime.format(dateString)!==storeEnd.format(dateString)) {
                return
            } else if (dateTime.unix()>=storeStart.unix()&&storeEnd.unix()>=dateTime.unix()) {
                match=true;
                event={ hours, id: doc.id };
            }
        });
        if (match) {
            let { slots }=event.hours;
            dateTime=dateTime.unix();
            slots=slots.filter(date => moment(date.toDate()).unix()===dateTime)
            if (slots.length===0) {
                try {
                    const ref=await db.collection('events').doc(event.id)
                    const resp=await ref.update({
                        slots: admin.firestore.FieldValue.arrayUnion(admin.firestore.Timestamp.fromDate(moment.unix(dateTime).toDate()))
                    }, { merge: true });
                    if (resp)
                        res.status(200).send({ message: 'event created successfully' })
                } catch (error) {
                    console.log(error)
                    res.status(500).send({ message: 'something went wrong!', error: true });
                }
            } else {
                res.status(500).send({ message: 'Pick a valid slot!', error: true });
            }
        } else {
            console.log('res', res);
            res.status(404).send({ message: 'No slots available', error: true });
        }

    } catch (err) {
        console.log(err)
        res.status(500).send({ message: 'something went wrong!', error: true });
    }
}

module.exports.getSlots = getSlots;
module.exports.getEvents = getEvents;
module.exports.createEvent = createEvent;