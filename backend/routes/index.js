require('dotenv').config();      //access env variables
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app=express();
const { getSlots, getEvents, createEvent } = require('./firebase');
const PORT = process.env.PORT || 5000;

app.use(cors({ origin: true }));
app.use(bodyParser.json());

app.post('/freeSlots', getSlots);
app.post('/getEvents', getEvents);
app.post('/createEvent', createEvent);

app.listen(PORT, () => console.log(`server started ${PORT}`));
module.exports=app;