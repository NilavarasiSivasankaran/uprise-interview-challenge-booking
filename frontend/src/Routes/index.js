import React from "react";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link
} from "react-router-dom";
import BookSlot from '../Components/BookSlot';
import EmptySlot from '../Components/EmptySlot';
import Success from '../Components/Success';

const Routes=() => {
	return (
		<Router>
			<Switch>
				<Route path="/slots">
					<EmptySlot />
				</Route>
				<Route path="/success">
					<Success />
				</Route>
				<Route path="/">
					<BookSlot />
				</Route>
			</Switch>
		</Router>
	)
}

export default Routes;
