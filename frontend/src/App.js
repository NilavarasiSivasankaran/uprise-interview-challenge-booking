import React from 'react';
import Routes from './Routes';
import BookSlot from './Components/BookSlot';

function App() {
    return (
        <Routes>
            <BookSlot />
      </Routes>
  );
}

export default App;
