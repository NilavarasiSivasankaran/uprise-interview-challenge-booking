import axios from 'axios';
import {
    BASE_URL
} from '../config';

export const getEvents = async (data) => {
	try {
        const res = await axios.post(BASE_URL+'/getEvents', data, { validateStatus: false });
        return res;
	} catch (err) {
        return err;
	}
}

export const freeSlots = async (data) => {
	try {
        const res = await axios.post(BASE_URL+'/freeSlots', data, { validateStatus: false });
        return res;
	} catch (err) {
        return err;
	}
}

export const createEvent = async (data) => {
	try {
        const res = await axios.post(BASE_URL+'/createEvent', data, { validateStatus: false });
        return res;
	} catch (err) {
        return err;
	}
}
