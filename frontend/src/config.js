export const BASE_URL=process.env.REACT_APP_API_SERVER || ' http://localhost:3000';

export const durationLimit = [
	{ value: 30, label: '30 minutes' },
	{ value: 60, label: '60 minutes' }
];

export const timezoneOpt = [
	{ value: 'Australia/Sydney', label: 'Sydney' },
	{ value: 'America/Los_Angeles', label: 'Los Angeles' },
	{ value: 'America/New_York', label: 'New York' },
	{ value: 'Asia/Calcutta', label: 'Kolkata' }
];

export const dateString = 'YYYY/MM/DD';