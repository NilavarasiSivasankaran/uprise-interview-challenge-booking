import styled from 'styled-components';

export const SelectWrapper = styled.div`
    .css-1okebmr-indicatorSeparator{
        display: none;
    }
    .css-yk16xz-control{
        border-top: none !important;
        border-left: none !important;
        border-right: none !important;
        border-bottom: 2px solid #edeafa !important;
    }
`;