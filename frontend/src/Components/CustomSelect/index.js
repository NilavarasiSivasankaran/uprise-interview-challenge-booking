import React from 'react';
import Select from 'react-select';
import {
    SelectWrapper
} from './styles';

const CustomSelect = ({ options, setValue, value, ...props }) => {
	const customStyles = {
		control: (base, state) => ({
			...base,
			borderColor: "#fff",
			border: 0,
			borderBottom: "1px solid #edeafa",
			color: "#111",
			boxShadow: state.isFocused? null:null,
			"&:hover": {
				borderColor: "#edeafa"
			}
		}),
		menu: base => ({
			...base,
			borderColor: "#fff",
			borderRadius: 20,
			hyphens: "auto",
			marginTop: 0,
			textAlign: "left",
			wordWrap: "break-word"
		}),
		menuList: base => ({
			...base,
			padding: 0
		}),
		option: (base, state) => ({
			...base,
			background: state.isFocused? "#f8f8ff":"#fff",
			color: state.isSelected? "#7c60ff":"#9796a0",
		})
	}

	return (
		<SelectWrapper>
			<Select
				options={options}
				styles={customStyles}
				defaultValue={value}
				onChange={val => setValue(val.value)}
			/>
		</SelectWrapper>
	)
}

export default CustomSelect;
