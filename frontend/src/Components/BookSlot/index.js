import React, {
    useEffect,
    useState
} from 'react';
import {
    withRouter
} from 'react-router-dom';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import {
	DayPickerSingleDateController
} from 'react-dates';
import moment from 'moment';


import {
    ContainerFluid,
    Col
} from '@uprise/grid';
import { Button } from '@uprise/button';
import {
    P,
    Bold
} from '@uprise/text';
import {
    H2
} from '@uprise/headings';
import {
    Card
} from '@uprise/card';
import Icons from '@uprise/icons'


import cogoToast from 'cogo-toast';

import { getEvents, freeSlots as freeSlotsApi } from '../../Api';
import CustomSelect from '../CustomSelect';
import {
    Sections,
    ContainerWrapper,
    StyledDate,
    TitleBtn,
    HeaderWrap,
    ImgWrapper
} from './styles';
import {
    durationLimit,
    timezoneOpt,
    dateString
} from '../../config';


const BookSlot = ({ history }) => {

	const [date, setDate] = useState(moment());
	const [timezone, setTimezone] = useState();
	const [duration, setDuration] = useState();
	const [freeSlots, setFreeSlots] = useState([]);
	const [disabled, setDisabled] = useState([]);

    //const isBlocked = day => {
    //    return !disabled.some(el => day.format(dateString)===moment(el).format(dateString))
    //};
    const isBlocked=false;

    const eventsInnit = async () => {
        const tz = timezone||'America/New_York';
        try {
        	const res = await getEvents({
        		startDate: moment().tz(tz).startOf('month').format(dateString),
        		endDate: moment().tz(tz).endOf('month').format(dateString)
        	});
            setDisabled(res.data.data);
        } catch (err) {
            console.log(err);
        	cogoToast.error('No Slots Available');
        }
    };

    const getFreeSlots = async () => {

        if (!duration)
            cogoToast.error('Please select a duration');
        else if (!timezone)
            cogoToast.error('Please select a timezone');

        else {
            freeSlotsApi({
                timezone,
                date: moment(date).format(dateString)
            }).then(res => {
                if (res.data.error) {
                    cogoToast.error(res.data.message);
                    setFreeSlots([]);
                } else {
                    setFreeSlots(res.data.data.slots);
                    history.push({
                        pathname: '/slots',
                        freeSlots: res.data.data.slots,
                        duration: res.data.data.duration,
                        timezone,
                    });
                }
            }).catch(err => {
                cogoToast.error('Something went wrong!');
                setFreeSlots([]);
                console.error(err);
            });
        }
    };

    useEffect(() => {
        eventsInnit();
    }, [timezone]);


	return (
		<ContainerWrapper>
			<HeaderWrap>
				<ContainerFluid className="d-flex justify-content-center">
                    <ImgWrapper>
                        <img src={Icons.chevronRightPurple} />
						<img src="/uprise_logo.png" />
					</ImgWrapper>
					<H2>Book a Call</H2>
				</ContainerFluid>
			</HeaderWrap>
			<ContainerFluid>
				<Col className="d-flex justify-content-center">
					<Card
						width={"900px"}
						shadow={true}
						padding={"40px"}
					>
						<Sections>
                            <P><Bold>Select a date</Bold></P>
							<StyledDate>
								<DayPickerSingleDateController
									date={date}
									focused
									numberOfMonths={1}
									onDateChange={(val) => setDate(val)}
									navNext={<img src={Icons.chevronRightPurple} />}
									navPrev={<img src={Icons.chevronRightPurple} />}
									hideKeyboardShortcutsPanel={true}
									//isDayBlocked={isBlocked}
								/>
							</StyledDate>
						</Sections>
						<Sections>
							<Col>
								<P><Bold>Select your timezone</Bold></P>
								<CustomSelect
									options={timezoneOpt}
									value={timezone}
									setValue={setTimezone}
								/>
								<P><Bold>Select call duration</Bold></P>
								<CustomSelect
									options={durationLimit}
									value={duration}
									setValue={setDuration}
								/>
							</Col>
						</Sections>
						<TitleBtn>
							<Button
								title={"Get Free Slots"}
								onClick={() => getFreeSlots()}
							/>
						</TitleBtn>
					</Card>
				</Col>
			</ContainerFluid>
		</ContainerWrapper>
	)
}

export default withRouter(BookSlot);
