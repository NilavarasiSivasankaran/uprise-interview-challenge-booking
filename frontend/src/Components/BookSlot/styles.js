import styled from 'styled-components'

export const Sections = styled.div`
    width: 370px;
    float: left;
    margin-bottom: 20px;
`;

export const ContainerWrapper = styled.div`
    background: #f8f8ff;
    min-height: 100vh;
    .col-12{
        padding-top: 25px;
    }
    .col-12 div{
        background: #fff;
    }
`;

export const StyledDate = styled.div`
    .CalendarDay__selected, .CalendarDay__selected:active, .CalendarDay__selected:hover{
        background: #7d60ff;
        border: 1px solid #7d60ff;
    }
    .DayPickerNavigation_button{
        left: 30px;
        top: 25px;
        position: absolute;
    }
    .DayPickerNavigation_button:nth-of-type(1) img{
        transform: rotate(180deg);
    }
    .DayPickerNavigation_button:nth-of-type(2){
        left: 285px;
        top: 25px;
        position: absolute;
    }
    .CalendarDay__default{
        border: none;
    }
    .CalendarDay__blocked_calendar, .CalendarDay__blocked_calendar:active, .CalendarDay__blocked_calendar:hover{
        color: #cccbd0; 
        border: none;
        background: #fff;
    }
`;

export const TitleBtn = styled.div`
    margin-top: 20px;
`;

export const HeaderWrap = styled.div`
    background: #fff;
    padding-top: 20px;
    position: relative;
    h2{
        margin-top: 0;
        margin-bottom: 5px;
    }
`;

export const ImgWrapper = styled.div`
    img:nth-of-type(1){
        position: absolute;
        top: 14px;
        left: 40px;
    }
`;