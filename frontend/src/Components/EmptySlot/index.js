import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	ContainerFluid,
	Col
} from '@uprise/grid';
import { Button } from '@uprise/button';
import {
	P,
	Bold
} from '@uprise/text';
import {
	H2
} from '@uprise/headings';
import { Card } from '@uprise/card';
import moment from 'moment';
import Icons from '@uprise/icons';
import { createEvent } from '../../Api';
import cogoToast from 'cogo-toast';
import {
	SlotWrapper,
	ContainerWrapper,
	HeaderWrap,
	ImgWrapper,
} from './styles';


const EmptySlot = ({ location, history }) => {
	let freeSlots=location.freeSlots||[];
	const { timezone, duration }=location;

	const timeString='HH:mm A';

	let tempSlots=[{
		time: '10-11AM',
		idx: 1
	}];

	console.log('moment()', moment(), timezone, location);
	let timezone1="America/Los_Angeles";
	const begin=moment().tz(timezone1).format(timeString);
	const end=moment().add(duration, 'minutes').tz(timezone1).format(timeString);
	tempSlots.push(`${begin} - ${end}`);
	console.log('tempSlots', begin, end, tempSlots);

	if (freeSlots.length) {
		const { duration } = location;
		freeSlots.map(date => {
			const begin = moment(date).tz(timezone).format(timeString);
			const end = moment(date).add(duration, 'minutes').tz(timezone).format(timeString);
			tempSlots.push(`${begin} - ${end}`);
		});
	}

	const createEventCall = async (slot, idx) => {
		console.log('duration, dateTime, timezone');
		let dateTime = freeSlots[idx];
		createEvent({
			duration,
			dateTime,
			timezone,
		}).then((res => {
			if (res.data.error) {
				cogoToast.error(res.data.message);
			} else {
				history.push('/success');
			}
		}))
			.catch(err => {
				cogoToast.error('Something went wrong');
				console.log(err)
			});

	};

	return (
		<ContainerWrapper>
			<HeaderWrap>
				<ContainerFluid className="d-flex justify-content-center">
					<ImgWrapper>
						<img src={Icons.chevronRightPurple} onClick={() => history.push('/')} />
						<img src="/uprise_logo.png" />
					</ImgWrapper>
					<H2>Available Slots</H2>
				</ContainerFluid>
			</HeaderWrap>
			<ContainerFluid>
				<Col className="d-flex justify-content-center">
					<Card
						width={"800px"}
						shadow={true}
						padding={"30px"}
					>
						{
							freeSlots.length?
								<>
									<P><Bold>Click any of these slots to book</Bold></P>
									<SlotWrapper>
										{
											tempSlots.map((time, idx) => (
												<Button
													title={time}
													variant={"tertiary"}
													key={idx}
													onClick={(time) => createEventCall(time, idx)}
												/>
											))
										}
									</SlotWrapper>
								</>
								:
								<P><Bold>No slots available</Bold></P>
						}
					</Card>
				</Col>
			</ContainerFluid>
		</ContainerWrapper>

	)
}

export default withRouter(EmptySlot);
