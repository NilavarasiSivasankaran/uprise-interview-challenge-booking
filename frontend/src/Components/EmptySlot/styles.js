import styled from 'styled-components';

export const SlotWrapper=styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    button{
        margin: 15px;
        width: 90%;
    }
    button:hover{
        background-color: rgb(125,96,255);
        color: white;
    }

`;

export const ContainerWrapper=styled.div`
    background: #f8f8ff;
    min-height: 100vh;
    .col-12{
        padding-top: 25px;
    }
    .col-12 div{
        background: #fff;
    }
`;

export const HeaderWrap=styled.div`
    background: #fff;
    padding-top: 20px;
    position: relative;
    h2{
        margin-top: 0;
        margin-bottom: 5px;
    }
`;

export const ImgWrapper=styled.div`
    img:nth-of-type(1){
        transform: rotate(180deg);
        position: absolute;
        top: 27px;
        left: 20px;
    }
    img:nth-of-type(2){
        position: absolute;
        top: 14px;
        left: 40px;
    }
`;
