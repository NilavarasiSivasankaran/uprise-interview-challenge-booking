import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	ContainerFluid,
	Col
} from '@uprise/grid';
import {
	Bold
} from '@uprise/text';
import {
	H2,
	H3
} from '@uprise/headings';
import { Card } from '@uprise/card';
import Icons from '@uprise/icons';

import {
	ImgWrapper,
	HeaderWrap,
	CardWrapper,
	ContainerWrapper
} from './styles';

const Success=({ history, ...props }) => {

	return (
		<ContainerWrapper>
			<HeaderWrap>
				<ContainerFluid className="d-flex justify-content-center">
					<ImgWrapper>
						<img src={Icons.chevronRightPurple} onClick={() => history.push('/')} />
						<img src="/uprise_logo.png" />
					</ImgWrapper>
					<H2>Booking a Coaching</H2>
				</ContainerFluid>
			</HeaderWrap>
			<ContainerFluid>
				<Col className="d-flex justify-content-center">
					<CardWrapper>
						<Card
							width={"800px"}
							shadow={true}
							padding={"30px"}
						>
							<img src="/success_tick.png" height="90" />
							<H3><Bold>Booking successful!</Bold></H3>
						</Card>
					</CardWrapper>
				</Col>
			</ContainerFluid>
		</ContainerWrapper>

	)

}

export default withRouter(Success);
